//
//  UITableView+Extension.swift
//  MarvelWallExample
//
//  Created by Julian Rodrigo Arredondo Escobar on 22/11/21.
//

import UIKit

extension UITableView {
    
    func registerUINib(nibName: String,
                        bundle: Bundle = .main) {
        register(UINib(nibName: nibName, bundle: bundle),
                 forCellReuseIdentifier: nibName)
    }
}
