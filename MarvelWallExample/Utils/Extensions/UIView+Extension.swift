//
//  UIView+Extension.swift
//  MarvelWallExample
//
//  Created by Julian Rodrigo Arredondo Escobar on 20/11/21.
//

import UIKit

extension UIView {
    class var viewID: String {
        return "\(self)"
    }
}
