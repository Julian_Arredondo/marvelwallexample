//
//  UICollectionView.swift
//  MarvelWallExample
//
//  Created by Julian Rodrigo Arredondo Escobar on 20/11/21.

import UIKit

extension UICollectionView {
    func registerUINib(nibName: String,
                        bundle: Bundle = .main) {
        register(UINib(nibName: nibName, bundle: bundle),
                 forCellWithReuseIdentifier: nibName)
    }
}
