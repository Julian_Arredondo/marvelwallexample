//
//  MarvelBaseViewcontroller.swift
//  MarvelWallExample
//
//  Created by Julian Rodrigo Arredondo Escobar on 20/11/21.
//

import Foundation
import UIKit

protocol MarvelBaseViewControllerProtocol: AnyObject {
    func showLoadingView()
    func hideLoadingView()
}

class MarvelBaseViewController: UIViewController {
        
    // MARK: - Var Let
    private var activityIndicatorView: UIActivityIndicatorView?
    private let backgroundView = UIView()
    
    // MARK: - Lifecycle
    override func viewDidLoad () {
        super.viewDidLoad()
        setupActivityIndicatorView()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        backgroundView.frame = self.view.bounds
        backgroundView.center = self.view.center
        activityIndicatorView?.frame = self.view.bounds
     }
    
    private func setupActivityIndicatorView() {
        activityIndicatorView = UIActivityIndicatorView(frame: self.view.bounds)
        backgroundView.layer.cornerRadius = 05
        backgroundView.clipsToBounds = true
        backgroundView.isOpaque = false
        backgroundView.backgroundColor = UIColor(white: 0.0, alpha: 0.6)
        
        activityIndicatorView?.style = UIActivityIndicatorView.Style.medium
        activityIndicatorView?.color = UIColor.white
        backgroundView.addSubview(activityIndicatorView ?? UIActivityIndicatorView())
        self.navigationController?.view.addSubview(backgroundView)
        backgroundView.isHidden = true
    }
}

// MARK: - Extension Protocol
extension MarvelBaseViewController: MarvelBaseViewControllerProtocol {
    
    func showLoadingView() {
        if activityIndicatorView == nil {
            setupActivityIndicatorView()
        }
        
        activityIndicatorView?.startAnimating()
        backgroundView.isHidden = false
    }
    
    func hideLoadingView() {
        activityIndicatorView?.stopAnimating()
        backgroundView.isHidden = true
    }
}
