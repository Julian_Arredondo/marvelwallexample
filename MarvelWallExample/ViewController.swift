//
//  ViewController.swift
//  MarvelWallExample
//
//  Created by Julian Rodrigo Arredondo Escobar on 15/11/21.
//

import UIKit

class ViewController: UIViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        UINavigationBar.appearance().setBackgroundImage(UIImage(), for: UIBarPosition.any, barMetrics: UIBarMetrics.default)
            UINavigationBar.appearance().shadowImage = UIImage()
        UINavigationBar.appearance().tintColor = .black
        UINavigationBar.appearance().barTintColor = .red
        UINavigationBar.appearance().backgroundColor = .red
        UINavigationBar.appearance().clipsToBounds = false
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        let accountProductInfoView = MarvelCharactersRouter.createMarvelCharactersModule()
        let navController = UINavigationController(rootViewController: accountProductInfoView)
        navController.modalPresentationStyle = .fullScreen
        self.present(navController, animated: true)
    }
}
