//
//  NetworkingMarvelCharactersProtocols.swift
//  MarvelWallExample
//
//  Created by Julian Rodrigo Arredondo Escobar on 20/11/21.
//  Copyright (c) 2021 Personal. All rights reserved.
//

import Foundation

enum ResultResponseStatus<T, U> {
    case success(T)
    case failure(U)
}

protocol NetworkingMarvelCharactersAdapterProtocol: AnyObject {
    var service: NetworkingMarvelCharactersServiceProtocol { get set }
    func getCharacters(nameStartsWith: String, offset: Int, completion: @escaping (ResultResponseStatus<MarvelCharacterResultModel, Error>) -> Void)
    func getCharacteresByID(id: Int, completion: @escaping (ResultResponseStatus<CharacterDetailResponse, Error>) -> Void)
}

protocol NetworkingMarvelCharactersRouterProtocol: AnyObject {
    associatedtype Router: NetworkRouter
    var router: Router { get set }
   
}

protocol NetworkingMarvelCharactersServiceProtocol: AnyObject {
    func getCharacters(nameStartsWith: String,
                       offset: Int,
                       completion: @escaping NetworkRouterCompletion)
    func getCharacteresByID(id: Int,
                       completion: @escaping NetworkRouterCompletion)
}
