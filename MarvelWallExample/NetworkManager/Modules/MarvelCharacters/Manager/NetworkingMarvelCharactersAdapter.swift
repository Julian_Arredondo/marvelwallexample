//
//  NetworkingMarvelCharactersAdapter.swift
//  MarvelWallExample
//
//  Created by Julian Rodrigo Arredondo Escobar on 20/11/21.
//  Copyright (c) 2021 Personal. All rights reserved.
//

import Foundation

final class NetworkingMarvelCharactersAdapter: DefaultNetworkManager<RouterMarvel<NetworkingMarvelCharactersAPI>>, NetworkingMarvelCharactersAdapterProtocol {
   
    var service: NetworkingMarvelCharactersServiceProtocol
    
    init(_ service: NetworkingMarvelCharactersServiceProtocol = NetworkingMarvelCharactersRouter(RouterMarvel<NetworkingMarvelCharactersAPI>())) {
        self.service = service
        super.init((service as? NetworkingMarvelCharactersRouter<Router>)?.router)
    }
    
    func getCharacters(nameStartsWith: String, offset: Int, completion: @escaping (ResultResponseStatus<MarvelCharacterResultModel, Error>) -> Void) {
        service.getCharacters(nameStartsWith: nameStartsWith, offset: offset) { [weak self] (data, response, error) in
            guard let self = self, let response = response else {
                completion(.failure(ErrorType.failed))
                return
            }
            
            let result = self.handleNetwork(response, data: data)
            switch result {
            case .success:
                do {
                    let characterDataWrapper = try JSONDecoder().decode(MarvelCharacterDataResponse.self, from: data ?? Data())
                    let characterResult = MarvelCharacterResponseMapper().loadMarvelCharacterDataResponse(characterDataWrapper)
                     completion(.success(characterResult))
                 } catch {
                     completion(.failure(ErrorType.failed))
                 }
            case .failure( _):
                completion(.failure(ErrorType.failed))
            }
        }
    }
    
    func getCharacteresByID(id: Int, completion: @escaping (ResultResponseStatus<CharacterDetailResponse, Error>) -> Void) {
        service.getCharacteresByID(id: id) { [weak self] (data, response, error) in
            guard let self = self, let response = response else {
                completion(.failure(ErrorType.failed))
                return
            }
            
            let result = self.handleNetwork(response, data: data)
            switch result {
            case .success:
                do {
                    let content: CharacterDetailResponse = try self.load(data: data)
                    completion(.success(content))
                 } catch {
                     completion(.failure(ErrorType.failed))
                 }
            case .failure( _):
                completion(.failure(ErrorType.failed))
            }
        }
    }

    enum ErrorType: Error, LocalizedError {
        case unauthorized
        case notFound
        case failed
        
        var errorDescription: String? {
            return "Error"
        }
    }
}
