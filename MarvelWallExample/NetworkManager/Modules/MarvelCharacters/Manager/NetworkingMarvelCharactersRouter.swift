//
//  NetworkingMarvelCharactersRouter.swift
//  MarvelWallExample
//
//  Created by Julian Rodrigo Arredondo Escobar on 20/11/21.
//  Copyright (c) 2021 Personal All rights reserved.
//

import Foundation

final class NetworkingMarvelCharactersRouter<Router: NetworkRouter>: BaseNetworkingRouter<Router>, NetworkingMarvelCharactersRouterProtocol, NetworkingMarvelCharactersServiceProtocol where Router.EndPoint == NetworkingMarvelCharactersAPI {
    func getCharacteresByID(id: Int, completion: @escaping NetworkRouterCompletion) {
        router.request(.getCharacteresByID(id: id)) { (data, response, error) in
            completion(data, response, error)
        }
    }
    
    func getCharacters(nameStartsWith: String, offset: Int, completion: @escaping NetworkRouterCompletion) {
        router.request(.getCharacteres(nameStartsWith: nameStartsWith, offset: offset)) { (data, response, error) in
            completion(data, response, error)
        }
    }
}
