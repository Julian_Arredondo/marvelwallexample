//
//  MarvelCharacterNetUserCase.swift
//  MarvelWallExample
//
//  Created by Julian Rodrigo Arredondo Escobar on 20/11/21.
//

import Foundation

struct MarvelCharacterDataResponse: Codable {
    let code: Int?
    let data: MarvelCharacterData?
}

struct MarvelCharacterData: Codable {
    let results: [MarvelCharacterResult]?
}

struct MarvelCharacterResult: Codable {
    let id: Int?
    let name: String?
    let description: String?
    let modified: String?
    let resourceURI: String?
    let thumbnail: MarvelCharacterResultImage
    
    struct MarvelCharacterResultImage: Codable {
        let thumbnailpath: String?
        let thumbnailExtension: String?
        enum CodingKeys: String, CodingKey {
            case thumbnailpath = "path"
            case thumbnailExtension = "extension"
        }
    }
}

struct MarvelCharacterResponseMapper {
    func loadMarvelCharacterDataResponse(_ response: MarvelCharacterDataResponse) -> MarvelCharacterResultModel {
        var marvelCharacterResultUseCase = MarvelCharacterResultModel()
        marvelCharacterResultUseCase.characters = [MarvelCharacterModel]()
    
        if let results = response.data?.results {
            let _ = results.compactMap({ (allResults) in
                let characterModel = loadMarvelCharacter(allResults)
                marvelCharacterResultUseCase.characters?.append(characterModel)
            })
        }
        
        return marvelCharacterResultUseCase
    }
    
    func loadMarvelCharacter(_ marvelCharacterModel: MarvelCharacterResult) -> MarvelCharacterModel {
        var marvelCharacterUseCase = MarvelCharacterModel()
        marvelCharacterUseCase.id = marvelCharacterModel.id
        marvelCharacterUseCase.name = marvelCharacterModel.name
        marvelCharacterUseCase.description = marvelCharacterModel.description
        
        if let path = marvelCharacterModel.thumbnail.thumbnailpath, let imgExtension = marvelCharacterModel.thumbnail.thumbnailExtension {
            marvelCharacterUseCase.thumbnail = "\(path).\(imgExtension)"
        }
        
        return marvelCharacterUseCase
    }
}
