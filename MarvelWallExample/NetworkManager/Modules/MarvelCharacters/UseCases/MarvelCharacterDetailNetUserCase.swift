//
//  MarvelCharacterDetailNetUserCase.swift
//  MarvelWallExample
//
//  Created by Julian Rodrigo Arredondo Escobar on 21/11/21.
//

import Foundation

// MARK: - Result
struct CharacterDetailModel: Codable {
    let id: Int
    let name, resultDescription: String
    let thumbnail: Thumbnail
    let resourceURI: String
    let comics, series, stories: Sections
    let urls: [URLElement]

    enum CodingKeys: String, CodingKey {
        case id, name
        case resultDescription = "description"
        case thumbnail, resourceURI, comics, series, stories, urls
    }
}

// MARK: - Comics
struct Sections: Codable {
    let available: Int
    let collectionURI: String
    let items: [SectionsItem]
    let returned: Int
}

// MARK: - ComicsItem
struct SectionsItem: Codable {
    let resourceURI: String
    let name: String
}

// MARK: - Stories
struct Stories: Codable {
    let available: Int
    let collectionURI: String
    let items: [StoriesItem]
    let returned: Int
}

// MARK: - StoriesItem
struct StoriesItem: Codable {
    let resourceURI: String
    let name, type: String
}

// MARK: - Thumbnail
struct Thumbnail: Codable {
    let path: String
    let thumbnailExtension: String

    enum CodingKeys: String, CodingKey {
        case path
        case thumbnailExtension = "extension"
    }
}

// MARK: - URLElement
struct URLElement: Codable {
    let type: String
    let url: String
}

// MARK: - CharacterDetail
struct CharacterDetailResponse: Codable {
    let code: Int
    let status, copyright, attributionText, attributionHTML: String
    let etag: String
    let data: DataClass
}

// MARK: - DataClass
struct DataClass: Codable {
    let offset, limit, total, count: Int
    let results: [CharacterDetailModel]
}
