//
//  NetworkingMarvelCharactersAPI.swift
//  MarvelWallExample
//
//  Created by Julian Rodrigo Arredondo Escobar on 20/11/21.
//  Copyright (c) 2021 Personal. All rights reserved.
//

import Foundation

enum NetworkingMarvelCharactersAPI {
    case getCharacteres(nameStartsWith: String, offset: Int)
    case getCharacteresByID(id: Int)
}

extension NetworkingMarvelCharactersAPI: EndPointType {
    
    var path: String {
        switch self {
        case .getCharacteres:
            return "/characters"
        case .getCharacteresByID(let id):
            return "/characters/\(id)"
        }
    }
    
    var data: Data? {
        switch self {
        default:
            return nil
        }
    }
    
    var httpMethod: HTTPMethod {
        switch self {
        case .getCharacteres, .getCharacteresByID:
            return .get
        }
    }
    
    var parameters: Parameters? {
        let publicKey = "4dae12637d83a11eb7008188f3119ba1"
        let privateKey = "eca4edc48c302554244aa2088a819bda72129c00"
        let timestamp = String(Date().timeIntervalSince1970)
        let hash = (timestamp + privateKey + publicKey).md5
        var params = [
            "ts": timestamp,
            "apikey": publicKey,
            "hash": hash
        ] as [String : Any]
        switch self {
        case .getCharacteres(let name, let offset):
            params["offset"] = offset
            if name.count != 0 { params["nameStartsWith"] = name}
            return params
        case .getCharacteresByID(id: _):
            return params
        }
        
    }
    
}

