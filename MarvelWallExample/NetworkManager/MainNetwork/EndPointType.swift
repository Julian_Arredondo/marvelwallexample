//
//  EndPointType.swift
//  MarvelWallExample
//
//  Created by Julian Rodrigo Arredondo Escobar on 15/11/21.
//

import Foundation

enum HTTPResponseCodeStatus {
    case informational
    case success
    case redirection
    case clientError
    case serverError
    case unknown
    
    init(code: Int) {
        switch code {
        case 100...199:
            self = .informational
        case 200...299:
            self = .success
        case 300...399:
            self = .redirection
        case 400...499:
            self = .clientError
        case 500...599:
            self = .serverError
        default:
            self = .unknown
        }
    }
}

public enum MarvelErrorType: Error {
    case badRequest
    case errorService
    case nilFound
    case noRequest
}

enum HTTPMethod: String {
    case get = "GET"
    case post = "POST"
    case put = "PUT"
}

public typealias Parameters = [String: Any]
public typealias HTTPHeaders = [String: String]

protocol EndPointType {
    var baseURL: URL { get }
    var path: String { get }
    var data: Data? { get }
    var httpMethod: HTTPMethod { get }
    var parameters: Parameters? { get }
    var headers: HTTPHeaders? { get set }
}

extension EndPointType {
    var baseURL: URL {
        guard let url = URL(string: "https://gateway.marvel.com:443/v1/public") else { fatalError("baseURL could not be configured.")}
        return url
    }

    func encode<T>(_ value: T) -> Data? where T: Encodable {
        do {
            let json = JSONEncoder()
            return try json.encode(value)
        } catch {
            return nil
        }
    }
    
    var headers: HTTPHeaders? {
        get  {
            let headersResult = ["Content-Type": "application/json"]
            return headersResult
        }
        set { }
    }
}
