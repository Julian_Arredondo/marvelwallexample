//
//  NetworkManager.swift
//  MarvelWallExample
//
//  Created by Julian Rodrigo Arredondo Escobar on 15/11/21.
//

import Foundation

enum Result<T> {
    case success
    case failure(T)
}

protocol DefaultNetworkManagerProtocol {
    associatedtype Router: NetworkRouter
    var networkRouter: Router? { get set }
    func customHandler(_ response: HTTPURLResponse, data: Data?) -> Result<Error>?
}

enum DefaultErrorType: Error, LocalizedError {
    case notDecodeData
    case notParseData
    case failed
    case internalServerError(errorCode: Int)
}

enum ErrorHandlerType: String {
    case old
    case new
}

class DefaultNetworkManager<Router: NetworkRouter>: DefaultNetworkManagerProtocol {
    var networkRouter: Router?
    private var urls: [String] = []
    private var callerModuleView: String?
    
    init(_ networkRouter: Router? = RouterMarvel<Router.EndPoint>() as? Router, resetErrorRetries: Bool = true) {
        self.networkRouter = networkRouter
    }
    
    func load<T: Decodable> (data: Data?, as type: T.Type = T.self) throws -> T {
        guard let data = data else { throw DefaultErrorType.notDecodeData }
        
        do {
            let decoder = JSONDecoder()
            return try decoder.decode(T.self, from: data)
        } catch {
            print("ERROR PARSE DATA: \(error)")
            throw DefaultErrorType.notParseData
        }
    }
    
    final func handleNetwork(_ response: HTTPURLResponse, data: Data? = nil, errorHandlerType: ErrorHandlerType = .new) -> Result<Error> {
        switch response.statusCode {
        case 200...299:
            if let url = response.url?.absoluteString, urls.contains(url) {
                urls.removeAll { $0 == url }
            }
            return .success
        case 400, 404, 500, 502:
            return .failure(DefaultErrorType.internalServerError(errorCode: response.statusCode))
        default:
            return .failure(DefaultErrorType.failed)
        }
    }
    
    func customHandler(_ response: HTTPURLResponse, data: Data? = nil) -> Result<Error>? {
        return nil
    }
}
