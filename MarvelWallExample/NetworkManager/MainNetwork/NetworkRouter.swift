//
//  NetworkRouter.swift
//  MarvelWallExample
//
//  Created by Julian Rodrigo Arredondo Escobar on 15/11/21.
//

import Foundation

public typealias CompletionData = (Data?, HTTPURLResponse?, Error?)
public typealias NetworkRouterCompletion = (_ data: Data?, _ response: HTTPURLResponse?, _ error: Error?) -> Void

protocol NetworkRouter: class {
    associatedtype EndPoint: EndPointType
    
    init()
    func request(_ route: EndPoint, completion: @escaping NetworkRouterCompletion)
}

protocol NetworkingRouterProtocol {
    associatedtype Router: NetworkRouter
    var router: Router { get set }
}

class BaseNetworkingRouter<Router: NetworkRouter>: NetworkingRouterProtocol {
    var router: Router
    
    init(_ router: Router) {
        self.router = router
    }
}

class RouterMarvel<EndPoint: EndPointType>: NetworkRouter {
    var task: URLSessionTask?
    let session = URLSession.shared
    
    required init() {}
    
    deinit {
        task?.cancel()
    }
    
    func request(_ route: EndPoint, completion: @escaping NetworkRouterCompletion) {
        setupInterceptor(route) { (data, response, error) in
            completion(data, response, error)
        }
    }
    
    func setupInterceptor(_ route: EndPoint, completion: @escaping NetworkRouterCompletion) {
        let fullURL = route.baseURL.appendingPathComponent(route.path).absoluteString.removingPercentEncoding
        let url = URL(string: fullURL ?? String()) ?? route.baseURL.appendingPathComponent(route.path)
        var urlRequest = URLRequest(url: url)
        let headers = route.headers
        urlRequest.httpMethod = route.httpMethod.rawValue
        urlRequest.httpBody = route.httpMethod == .get ? nil : route.data
        urlRequest.allHTTPHeaderFields = headers
        if route.httpMethod == .get {
            var newURL = URLComponents(string: urlRequest.url?.absoluteString ?? String())
            newURL?.queryItems = route.parameters?.map({ (key: String, value: Any) -> URLQueryItem in
                URLQueryItem(name: key, value: "\(value)")
            })
            urlRequest.url = newURL?.url
        }

        session.configuration.timeoutIntervalForRequest = 120
        session.configuration.timeoutIntervalForResource = 120
        
        doNetworkTask(with: urlRequest) { [weak self](data, response, error) in
            
            guard let self = self else {
                completion(nil, nil, MarvelErrorType.errorService)
                return
            }

            let unwrapped = self.unwrapParameters(parameters: (data, response, error))
            guard unwrapped.result else {
                completion(nil, nil, MarvelErrorType.nilFound)
                return
            }
    
            self.validateServiceResponse(route, data: unwrapped.values.data, response: unwrapped.values.response,
                                         completion: { (data, response, error) in
                                            completion(data, response, error)
                                         })
        }
    }
    
    func unwrapParameters(parameters: CompletionData) ->
    (result: Bool, values: (data: Data, response: HTTPURLResponse)) {
        guard let availableData = parameters.0, let availableResponse = parameters.1, parameters.2 == nil else {
            return (false, (Data(), HTTPURLResponse()))
        }
        return (true, (availableData, availableResponse))
    }
    
    func doNetworkTask(with request: URLRequest, completion: @escaping NetworkRouterCompletion) {
        let task = session.dataTask(with: request) { data, response, error in
            DispatchQueue.main.async {
                completion(data, response as? HTTPURLResponse, error)
            }
        }
        task.resume()
    }
    
    private func validateServiceResponse(_ route: EndPoint, data: Data, response: HTTPURLResponse,
                                         completion: @escaping NetworkRouterCompletion) {
        print(response.statusCode)
        print(response)
        guard HTTPResponseCodeStatus(code: response.statusCode) != .success else {
            completion(data, response, nil)
            return
        }
        
        completion(nil, nil, MarvelErrorType.nilFound)
    }
    
    func validateWebContentRequest(_ route: EndPoint, completion: @escaping NetworkRouterCompletion) {
        var urlRequest = URLRequest(url: route.baseURL)
        urlRequest.httpMethod = route.httpMethod.rawValue
        urlRequest.cachePolicy = .reloadIgnoringLocalAndRemoteCacheData
        
        doNetworkTask(with: urlRequest) { (data, response, error) in
            if data?.count == .zero {
                completion(nil, nil, MarvelErrorType.nilFound)
            } else {
                completion(data, response, nil)
            }
        }
    }
}
