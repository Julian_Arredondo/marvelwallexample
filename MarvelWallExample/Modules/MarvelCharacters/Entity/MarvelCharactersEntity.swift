//
//  MarvelCharactersEntity.swift
//  MarvelWallExample
//
//  Created by Julian Rodrigo Arredondo Escobar on 20/11/21.
//

import Foundation

struct MarvelCharacterResultModel {
    var characters: [MarvelCharacterModel]?
}

struct MarvelCharacterModel {
    var id: Int?
    var name: String?
    var description: String?
    var thumbnail: String?
}
