//
//  MarvelCharactersPresenter.swift
//  MarvelWallExample
//
//  Created by Julian Rodrigo Arredondo Escobar on 15/11/21.
//  Copyright (c) 2021 Personal All rights reserved.
//

final class MarvelCharactersPresenter  {
    
    // MARK: - Var Let
    weak var view: MarvelCharactersViewProtocol?
    var router: MarvelCharactersRouterProtocol?
    var interactor: MarvelCharactersInteractorProtocol?
    private var searchName = ""
    private let limitPagination: Int = 20
    private var currentPage: Int = .zero
    
    lazy var marvelCharacterArrayModel = [MarvelCharacterModel]()
    
    func viewDidLoad() {
        fetchMarvelCharacters(nameStartsWith: searchName, offset: currentPage)
    }
    
    func resetPagination() {
        currentPage = .zero
        marvelCharacterArrayModel = []
    }
}

// MARK: - Extension PresenterProtocol
extension MarvelCharactersPresenter: MarvelCharactersPresenterProtocol {
    func getNumberOfCharacter() -> Int {
        return marvelCharacterArrayModel.count
    }
    
    func getCharacterByIndex(index: Int) -> MarvelCharacterModel? {
        if marvelCharacterArrayModel.count <= index {
            return nil
        }
        
        return marvelCharacterArrayModel[index]
    }
    
    func getSearchText() -> String {
        return searchName
    }
    
    func setSearchText(text: String) {
        resetPagination()
        searchName = text
        fetchMarvelCharacters(nameStartsWith: searchName, offset: currentPage)
    }
    
    func fetchCharacterDetail(id: Int) {
        view?.showLoaderView()
        interactor?.getMarvelCharactersByID(id: id)
    }
    
    func fetchMarvelCharacters(nameStartsWith: String, offset: Int) {
        interactor?.getMarvelCharacters(nameStartsWith: searchName, offset: currentPage * limitPagination)
        currentPage += 1
        view?.showLoaderView()
    }
    
    func goToCharacterDetail(characterDetailEntity: CharacterDetailEntity) {
        router?.gotToChacarterDetail(characterDetailEntity: characterDetailEntity)
    }
}

// MARK: - Extension InteractorOutputProtocol
extension MarvelCharactersPresenter: MarvelCharactersInteractorOutputProtocol {
    func onFetchMarvelCharactersSuccess(marvelCharacterModel: MarvelCharacterResultModel) {
        marvelCharacterArrayModel.append(contentsOf: marvelCharacterModel.characters ?? [])
        view?.reloadInfo()
    }
    
    func onFetchMarvelCharactersError() {
        //UHP to view
    }
    
    func onFetchMarvelCharactersDetailSuccess(characterDetailEntity: CharacterDetailEntity) {
        view?.hideLoaderView()
        goToCharacterDetail(characterDetailEntity: characterDetailEntity)
    }
    
    func onFetchMarvelCharactersDetailError() {
        //UHP to view
    }
}
