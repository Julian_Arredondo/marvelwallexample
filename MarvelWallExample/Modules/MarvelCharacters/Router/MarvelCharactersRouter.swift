//
//  MarvelCharactersRouter.swift
//  MarvelWallExample
//
//  Created by Julian Rodrigo Arredondo Escobar on 15/11/21.
//  Copyright (c) 2021 Personal. All rights reserved.
//

import UIKit

final class MarvelCharactersRouter: MarvelCharactersRouterProtocol {

    weak var viewController: MarvelBaseViewController?
    let transition = TransitionCoordinator()
    
    static func createMarvelCharactersModule() -> MarvelCharactersViewController {
        let ref = MarvelCharactersViewController(nibName: "MarvelCharactersViewController", bundle: nil)
        
        let presenter: MarvelCharactersPresenterProtocol & MarvelCharactersInteractorOutputProtocol = MarvelCharactersPresenter()
        
        let router = MarvelCharactersRouter()
        router.viewController = ref
        
        let interactor = MarvelCharactersInteractor()
        interactor.presenter = presenter
        
        presenter.view = ref
        presenter.router = router
        presenter.interactor = interactor
        
        ref.presenter = presenter
        return ref
    }
    
    func gotToChacarterDetail(characterDetailEntity: CharacterDetailEntity) {
        let marvelCharactersDetail = MarvelCharactersDetailRouter.createMarvelCharactersDetailModule(characterDetailEntity: characterDetailEntity)
        viewController?.modalPresentationStyle = .fullScreen
        viewController?.navigationController?.delegate = transition
        viewController?.navigationController?.show(marvelCharactersDetail, sender: nil)
    }
}

