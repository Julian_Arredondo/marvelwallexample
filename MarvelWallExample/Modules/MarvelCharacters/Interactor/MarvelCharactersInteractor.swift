//
//  MarvelCharactersInteractor.swift
//  MarvelWallExample
//
//  Created by Julian Rodrigo Arredondo Escobar on 15/11/21.
//  Copyright (c) 2021 Personal All rights reserved.
//

final class MarvelCharactersInteractor: MarvelCharactersInteractorProtocol {
    weak var presenter: MarvelCharactersInteractorOutputProtocol?
    
    var networkingMarvelCharactersAdapter: NetworkingMarvelCharactersAdapter?

    init(networkingMarvelCharactersAdapter: NetworkingMarvelCharactersAdapter = NetworkingMarvelCharactersAdapter()) {
        self.networkingMarvelCharactersAdapter = networkingMarvelCharactersAdapter
    }
    
    func getMarvelCharacters(nameStartsWith: String, offset: Int) {
        networkingMarvelCharactersAdapter?.getCharacters(nameStartsWith: nameStartsWith, offset: offset, completion: { [weak self] (result) in
            switch result {
            case .success(let data):
                self?.presenter?.onFetchMarvelCharactersSuccess(marvelCharacterModel: data)
            case .failure:
                self?.presenter?.onFetchMarvelCharactersError()
            }
        })
    }
    
    func getMarvelCharactersByID(id: Int) {
        networkingMarvelCharactersAdapter?.getCharacteresByID(id: id, completion: { [weak self] (result) in
            switch result {
            case .success(let response):
                let characterDetailEntity = CharacterDetailEntity (name: response.data.results.first?.name ?? "",
                                                                   description: response.data.results.first?.resultDescription ?? "",
                                                                   thumbnail: "\(response.data.results.first?.thumbnail.path ?? "").\(response.data.results.first?.thumbnail.thumbnailExtension ?? "")",
                                                                   sections: [CharacterDetailEntity.SectionsCharacterInfo(
                                                                                items: response.data.results.first?.comics.items.map{
                                                                                    $0.name
                                                                                } ?? [], nameSection: "Comics"),
                                                                              CharacterDetailEntity.SectionsCharacterInfo(
                                                                                items: response.data.results.first?.stories.items.map{
                                                                                    $0.name
                                                                                } ?? [], nameSection: "Stories"),
                                                                              CharacterDetailEntity.SectionsCharacterInfo(
                                                                                items: response.data.results.first?.series.items.map{
                                                                                    $0.name
                                                                                } ?? [], nameSection: "Series")])
                
                self?.presenter?.onFetchMarvelCharactersDetailSuccess(characterDetailEntity: characterDetailEntity)
            case .failure:
                self?.presenter?.onFetchMarvelCharactersDetailError()
            }
        })
    }
}
