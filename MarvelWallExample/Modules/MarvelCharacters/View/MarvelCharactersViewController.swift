//
//  MarvelCharactersViewController.swift
//  MarvelWallExample
//
//  Created by Julian Rodrigo Arredondo Escobar on 15/11/21.
//  Copyright (c) 2021 Personal. All rights reserved.
//
import UIKit
import CCBottomRefreshControl

final class MarvelCharactersViewController: MarvelBaseViewController {
    
    // MARK: - IBOutlets
    @IBOutlet weak var marvelCharacterCollection: UICollectionView!
    
    // MARK: - Var Let
    var presenter: MarvelCharactersPresenterProtocol?
    private var marvelNameSearchController: UISearchController?
    private let collectionSectionHeight: CGFloat = 40.0
    private let paddingCollectionSectionWidth: CGFloat = 48
    private let collectionSectionItemHeight : CGFloat = 190
    private let insetForSectionCollectionSection = UIEdgeInsets(top: 16,
                                                                left: 16,
                                                                bottom: 16,
                                                                right: 16)
    private var bottomRefreshControl = UIRefreshControl()
    private var viewToAnimate = UIView()
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        loadPresenterViewLoad()
        setupCollectionView()
        setupSearchController()
    }

    private func loadPresenterViewLoad() {
        presenter?.viewDidLoad()
        showLoadingView()
    }
    
    private  func setupSearchController () {
        marvelNameSearchController = UISearchController(searchResultsController: nil)
        let searchBar = marvelNameSearchController?.searchBar
        searchBar?.tintColor = .black
        searchBar?.backgroundColor = UIColor.clear
        if let searchTextField = searchBar?.value(forKey: "searchField") as? UITextField {
            searchTextField.backgroundColor = UIColor.white
            searchTextField.textColor = UIColor.black
            let glassIconView = searchTextField.leftView as! UIImageView
            glassIconView.image = glassIconView.image?.withRenderingMode(.alwaysTemplate)
            glassIconView.tintColor = .red
            if let clearButton = searchTextField.value(forKey: "_clearButton") as? UIButton {
                let templateImage =  clearButton.imageView?.image?.withRenderingMode(.alwaysTemplate)
                clearButton.setImage(templateImage, for: [])
                clearButton.tintColor = .red
            }
        }
        searchBar?.delegate = self
        self.navigationItem.searchController = marvelNameSearchController
        self.navigationItem.hidesSearchBarWhenScrolling = false
    }
    
    func setupCollectionView() {
        marvelCharacterCollection.delegate = self
        marvelCharacterCollection.dataSource = self
        marvelCharacterCollection.registerUINib(nibName: MarvelCharactersCollectionCell.viewID)
        marvelCharacterCollection.backgroundColor = .clear
        bottomRefreshControl = UIRefreshControl()
        bottomRefreshControl.translatesAutoresizingMaskIntoConstraints = false
        bottomRefreshControl.triggerVerticalOffset = 80
        bottomRefreshControl.addTarget(self, action: #selector(nextPagination), for: .valueChanged)
        marvelCharacterCollection.bottomRefreshControl = bottomRefreshControl
    }
    
    @objc func nextPagination() {
        presenter?.viewDidLoad()
    }
}

// MARK: - Extension Protocol
extension MarvelCharactersViewController: MarvelCharactersViewProtocol {
    func showLoaderView() {
        self.showLoadingView()
    }
    
    func hideLoaderView() {
        self.hideLoadingView()
    }
    
    func reloadInfo() {
        bottomRefreshControl.endRefreshing()
        marvelCharacterCollection.reloadData()
        hideLoadingView()
    }
    
    private func prepareViewToAnimate(realIndexPath: IndexPath, collectionView: UICollectionView, usableIndexPath: IndexPath) {
        let cell: MarvelCharactersCollectionCell = collectionView.cellForItem(at: realIndexPath) as! MarvelCharactersCollectionCell
        viewToAnimate = cell.backgroundContainerView
    }
}

extension MarvelCharactersViewController: UISearchBarDelegate, UISearchControllerDelegate {
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        presenter?.setSearchText(text: searchBar.text ?? "")
        marvelNameSearchController?.isActive = false
        searchBar.text = presenter?.getSearchText()
        showLoadingView()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        if presenter?.getSearchText().count != .zero {
            presenter?.setSearchText(text: "")
            showLoadingView()
        }
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchBar.text = presenter?.getSearchText()
    }
}

extension MarvelCharactersViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width: collectionView.frame.width, height: collectionSectionHeight)
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        numberOfItemsInSection section: Int) -> Int {
        return presenter?.getNumberOfCharacter() ?? .zero
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return insetForSectionCollectionSection
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let collectionViewSize = collectionView.frame.size.width - paddingCollectionSectionWidth
        return CGSize(width: collectionViewSize/2, height: collectionSectionItemHeight)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let idCharacter = presenter?.getCharacterByIndex(index: indexPath.row)?.id else { return }
        prepareViewToAnimate(realIndexPath: indexPath, collectionView: collectionView, usableIndexPath: indexPath)
        presenter?.fetchCharacterDetail(id: idCharacter)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(
                withReuseIdentifier: MarvelCharactersCollectionCell.viewID,
                for: indexPath) as? MarvelCharactersCollectionCell else { return UICollectionViewCell() }
        
        guard let characterItem = presenter?.getCharacterByIndex(index: indexPath.row) else {
            return UICollectionViewCell()
        }
        
        cell.configCell(characterItem)
        return cell
    }
}

extension MarvelCharactersViewController: animTransitionable {
    var tableView: UITableView? {
        return nil
    }
    
    var viewCharacterInfo: UIView {
        return viewToAnimate
    }
}
