//
//  MarvelCharactersCollectionCell.swift
//  MarvelWallExample
//
//  Created by Julian Rodrigo Arredondo Escobar on 20/11/21.
//

import Foundation
import UIKit
import AlamofireImage

final class MarvelCharactersCollectionCell: UICollectionViewCell {

    // MARK: - IBOutlets
    @IBOutlet weak var characterImgView: UIImageView!
    @IBOutlet weak var characterNameLabel: UILabel!
    @IBOutlet weak var backgroundContainerView: UIView!
    
    // MARK: - Var Let
    private let gradientLayer: CAGradientLayer = CAGradientLayer()

    // MARK: - Lifecycle
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        setupCellLayout()
    }
    
    private func setupCellLayout() {
        clipsToBounds = true
        
        contentView.clipsToBounds = true
        contentView.layer.cornerRadius = 8.0
        contentView.layer.masksToBounds = true
    }
    
    func configCell(_ characterModel: MarvelCharacterModel) {
        if let name = characterModel.name {
            characterNameLabel.text = name
        }
        
        if let imgString = characterModel.thumbnail, let imgUrl = URL(string: imgString) {
            characterImgView.af.setImage(
                withURL: imgUrl,
                placeholderImage: UIImage(
                    named: "")
            )
        }
    }
}
