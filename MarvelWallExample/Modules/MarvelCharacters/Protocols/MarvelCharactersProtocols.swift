//
//  MarvelCharactersProtocols.swift
//  MarvelWallExample
//
//  Created by Julian Rodrigo Arredondo Escobar on 15/11/21.
//  Copyright (c) 2021 Personal. All rights reserved.
//
import UIKit

protocol MarvelCharactersInteractorProtocol: AnyObject {
    var presenter: MarvelCharactersInteractorOutputProtocol? { get set }
    func getMarvelCharacters(nameStartsWith: String, offset: Int)
    func getMarvelCharactersByID(id: Int)
}

protocol MarvelCharactersPresenterProtocol: AnyObject {
    var view: MarvelCharactersViewProtocol? { get set }
    var router: MarvelCharactersRouterProtocol? { get set }
    var interactor: MarvelCharactersInteractorProtocol? { get set }
    
    func viewDidLoad()
    func getNumberOfCharacter() -> Int
    func getCharacterByIndex(index: Int) -> MarvelCharacterModel?
    func getSearchText() -> String
    func setSearchText(text: String)
    func fetchCharacterDetail(id: Int)
}

protocol MarvelCharactersInteractorOutputProtocol: AnyObject {
    func onFetchMarvelCharactersSuccess(marvelCharacterModel: MarvelCharacterResultModel)
    func onFetchMarvelCharactersError()
    func onFetchMarvelCharactersDetailSuccess(characterDetailEntity: CharacterDetailEntity)
    func onFetchMarvelCharactersDetailError()
}

protocol MarvelCharactersRouterProtocol: AnyObject {
    var viewController: MarvelBaseViewController? { get set }
    
    static func createMarvelCharactersModule() -> MarvelCharactersViewController
    
    func gotToChacarterDetail(characterDetailEntity: CharacterDetailEntity)
}

protocol MarvelCharactersViewProtocol: AnyObject {
    var presenter: MarvelCharactersPresenterProtocol? { get set }
    
    func reloadInfo()
    func showLoaderView()
    func hideLoaderView()
}
