//
//  MarvelCharactersDetailEntity.swift
//  MarvelWallExample
//
//  Created by Julian Rodrigo Arredondo Escobar on 21/11/21.
//

import Foundation

struct CharacterDetailEntity {
    let name: String?
    let description: String?
    let thumbnail: String?
    let sections: [SectionsCharacterInfo]
   
    struct SectionsCharacterInfo {
        let items: [String]
        let nameSection: String
    }
}
