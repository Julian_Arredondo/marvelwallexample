//
//  MarvelCharactersDetailPresenter.swift
//  MarvelWallExample
//
//  Created by Julian Rodrigo Arredondo Escobar on 21/11/21.
//  Copyright (c) 2021 Personal. All rights reserved.
//


import Foundation

final class MarvelCharactersDetailPresenter: MarvelCharactersDetailPresenterProtocol {
    
    weak var view: MarvelCharactersDetailViewProtocol?
    var router: MarvelCharactersDetailRouterProtocol?
    var interactor: MarvelCharactersDetailInteractorProtocol?
    
    var characterDetailEntity: CharacterDetailEntity?
    
    func viewDidLoad() {}
    
    
    func getNumberOfSections() -> Int {
        return characterDetailEntity?.sections.count ?? .zero
    }
    
    func getNumberOfRows(at section: Int) -> Int {
        characterDetailEntity?.sections[section].items.count ?? .zero
    }
    
    func getCharacterDetailEntitySections(at section: Int) -> CharacterDetailEntity.SectionsCharacterInfo? {
        return characterDetailEntity?.sections[section]
    }
    
    func getMarvelCharacterName() -> String {
        return characterDetailEntity?.name ?? ""
    }
    
    func getMarvelCharacterImageThumbnail() -> String {
        return characterDetailEntity?.thumbnail ?? ""
    }
    
    func getMarvelCharacterDescription() -> String {
        return characterDetailEntity?.description ?? ""
    }
}

extension MarvelCharactersDetailPresenter: MarvelCharactersDetailInteractorOutputProtocol {}
