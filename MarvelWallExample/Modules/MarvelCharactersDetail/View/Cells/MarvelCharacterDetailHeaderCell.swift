//
//  MarvelCharacterDetailHeaderCell.xib.swift
//  MarvelWallExample
//
//  Created by Julian Rodrigo Arredondo Escobar on 22/11/21.
//

import Foundation

import UIKit
import AlamofireImage

final class MarvelCharacterDetailHeaderCell: UITableViewCell {

    // MARK: - IBOutlets
    @IBOutlet weak var characterImgView: UIImageView!
    @IBOutlet weak var characterNameLabel: UILabel!
    @IBOutlet weak var backgroundContainerView: UIView!
    @IBOutlet weak var characterDescriptionLabel: UILabel!
    
    // MARK: - Lifecycle
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func configCell(name: String?, description: String?, thumbnail: String?) {
        characterNameLabel.text = name
        characterDescriptionLabel.text = name
        
        if let imgString = thumbnail, let imgUrl = URL(string: imgString) {
            characterImgView.af.setImage(
                withURL: imgUrl,
                placeholderImage: UIImage(
                    named: "")
            )
        }
    }
}
