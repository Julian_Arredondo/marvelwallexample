//
//  MarvelCharactersDetailViewController.swift
//  MarvelWallExample
//
//  Created by Julian Rodrigo Arredondo Escobar on 21/11/21.
//  Copyright (c) 2021 Personal. All rights reserved.
//

import UIKit
import AlamofireImage

final class MarvelCharactersDetailViewController: MarvelBaseViewController {
    
    // MARK: - IBOutlets
    @IBOutlet weak var thumbnailImage: UIImageView!
    @IBOutlet weak var descriptionCharacterLabel: UILabel!
    @IBOutlet weak var characterDetailTable: UITableView!
    @IBOutlet weak var containerCharacterInfoView: UIView!
    var headerHeightConstraint: NSLayoutConstraint = NSLayoutConstraint()
    
    var maxHeaderHeight: CGFloat = 400;
    let minHeaderHeight: CGFloat = 0;
    var previousScrollViewHeight: CGFloat = 0
    var previousScrollOffset: CGFloat = 0
    
    // MARK: - Var Let
    var presenter: MarvelCharactersDetailPresenterProtocol?
    private let defaultReIdCell = "Cell"
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        loadPresenterViewLoad()
        setupTableView()
        setupHeaderCharacterInfo()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.maxHeaderHeight = containerCharacterInfoView.frame.height
        headerHeightConstraint = NSLayoutConstraint(
            item: containerCharacterInfoView as Any,
            attribute: NSLayoutConstraint.Attribute.height,
            relatedBy: NSLayoutConstraint.Relation.equal,
            toItem: nil,
            attribute: NSLayoutConstraint.Attribute.notAnAttribute,
            multiplier: 1, constant: maxHeaderHeight)
        view.addConstraints([headerHeightConstraint])
        self.updateHeader()
        setupStartAnimation()
    }
    
    private func setupTableView() {
        characterDetailTable.delegate = self
        characterDetailTable.dataSource = self
        characterDetailTable.registerUINib(nibName: MarvelCharacterDetailHeaderCell.viewID)
        characterDetailTable.backgroundColor = .clear
        characterDetailTable.alpha = .zero
        previousScrollViewHeight = characterDetailTable.contentSize.height
    }
    
    private func loadPresenterViewLoad() {
        presenter?.viewDidLoad()
    }
    
    private func setupHeaderCharacterInfo() {
        navigationItem.title  = presenter?.getMarvelCharacterName()
        thumbnailImage.alpha = .zero
        descriptionCharacterLabel.alpha = .zero
        descriptionCharacterLabel.text = presenter?.getMarvelCharacterDescription()
        if let imgString =  presenter?.getMarvelCharacterImageThumbnail(), let imgUrl = URL(string: imgString) {
            thumbnailImage.af.setImage(
                withURL: imgUrl,
                placeholderImage: UIImage(
                    named: "")
            )
        }
    }
    
    private func setupStartAnimation() {
        let hideViewsAnimation = {
            UIViewPropertyAnimator(duration: 1, curve: .easeOut) {
                self.thumbnailImage.alpha = 1
                self.descriptionCharacterLabel.alpha = 1
                self.characterDetailTable.alpha = 1
            }
        }()
        hideViewsAnimation.startAnimation()
    }
}

 // MARK: - Extension Protocol
extension MarvelCharactersDetailViewController: MarvelCharactersDetailViewProtocol {}

extension MarvelCharactersDetailViewController: UITableViewDataSource, UITableViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        defer {
            self.previousScrollViewHeight = scrollView.contentSize.height
            self.previousScrollOffset = scrollView.contentOffset.y
        }

        let heightDiff = scrollView.contentSize.height - self.previousScrollViewHeight
        let scrollDiff = (scrollView.contentOffset.y - self.previousScrollOffset)

        guard heightDiff == .zero else { return }

        let absoluteTop: CGFloat = .zero
        let absoluteBottom: CGFloat = scrollView.contentSize.height - scrollView.frame.size.height

        let isScrollingDown = scrollDiff > .zero && scrollView.contentOffset.y > absoluteTop
        let isScrollingUp = scrollDiff < .zero && scrollView.contentOffset.y < absoluteBottom

        if canAnimateHeader(scrollView) {
            var newHeight = self.headerHeightConstraint.constant
            if isScrollingDown {
                newHeight = max(self.minHeaderHeight, self.headerHeightConstraint.constant - abs(scrollDiff))
            } else if isScrollingUp {
                newHeight = min(self.maxHeaderHeight, self.headerHeightConstraint.constant + abs(scrollDiff))
            }

            if newHeight != self.headerHeightConstraint.constant {
                self.headerHeightConstraint.constant = newHeight
                self.updateHeader()
                self.setScrollPosition(self.previousScrollOffset)
            }
        }
    }

    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        self.scrollViewDidStopScrolling()
    }

    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        if !decelerate {
            self.scrollViewDidStopScrolling()
        }
    }

    func scrollViewDidStopScrolling() {
        let range = self.maxHeaderHeight - self.minHeaderHeight
        let midPoint = self.minHeaderHeight + (range / 2)

        if self.headerHeightConstraint.constant > midPoint {
            self.expandHeader()
        } else {
            self.collapseHeader()
        }
    }

    func canAnimateHeader(_ scrollView: UIScrollView) -> Bool {
        let scrollViewMaxHeight = scrollView.frame.height + self.headerHeightConstraint.constant - minHeaderHeight
        return scrollView.contentSize.height > scrollViewMaxHeight
    }

    func collapseHeader() {
        self.view.layoutIfNeeded()
        UIView.animate(withDuration: 0.2, animations: { [weak self] in
            self?.headerHeightConstraint.constant = self?.minHeaderHeight ?? .zero
            self?.updateHeader()
            self?.view.layoutIfNeeded()
        })
    }

    func expandHeader() {
        self.view.layoutIfNeeded()
        UIView.animate(withDuration: 0.2, animations: { [weak self] in
            self?.headerHeightConstraint.constant = self?.maxHeaderHeight ?? .zero
            self?.updateHeader()
            self?.view.layoutIfNeeded()
        })
    }

    func setScrollPosition(_ position: CGFloat) {
        self.characterDetailTable.contentOffset = CGPoint(x: self.characterDetailTable.contentOffset.x, y: position)
    }

    func updateHeader() {
        let range = self.maxHeaderHeight - self.minHeaderHeight
        let openAmount = self.headerHeightConstraint.constant - self.minHeaderHeight
        let percentage = openAmount / range

        self.thumbnailImage.alpha = percentage
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
            let model = presenter?.getCharacterDetailEntitySections(at: section)
            return model?.nameSection
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        guard let numberSection = presenter?.getNumberOfSections()  else { return .zero}
        return numberSection
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return presenter?.getNumberOfRows(at: section) ?? .zero
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let model = presenter?.getCharacterDetailEntitySections(at: indexPath.section)
            let cell = UITableViewCell(style: .value1, reuseIdentifier: defaultReIdCell)
            cell.textLabel?.text = model?.items[indexPath.row]
            return cell
    }
}

extension MarvelCharactersDetailViewController: animTransitionable {
    var viewCharacterInfo: UIView {
        self.view.setNeedsLayout()
        self.view.layoutIfNeeded()
        return containerCharacterInfoView
    }
}
