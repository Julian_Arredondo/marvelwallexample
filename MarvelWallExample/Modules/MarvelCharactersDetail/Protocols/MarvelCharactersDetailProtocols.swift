//
//  MarvelCharactersDetailProtocols.swift
//  MarvelWallExample
//
//  Created by Julian Rodrigo Arredondo Escobar on 21/11/21.
//  Copyright (c) 2021 Personal. All rights reserved.
//

import UIKit

protocol MarvelCharactersDetailInteractorProtocol: AnyObject {
    var presenter: MarvelCharactersDetailInteractorOutputProtocol? { get set }
}

protocol MarvelCharactersDetailPresenterProtocol: AnyObject {
    var view: MarvelCharactersDetailViewProtocol? { get set }
    var router: MarvelCharactersDetailRouterProtocol? { get set }
    var interactor: MarvelCharactersDetailInteractorProtocol? { get set }
    var characterDetailEntity: CharacterDetailEntity? { get set }
    
    func viewDidLoad()
    
    func getNumberOfSections() -> Int
    func getNumberOfRows(at section: Int) -> Int
    func getCharacterDetailEntitySections(at section: Int) -> CharacterDetailEntity.SectionsCharacterInfo?
    func getMarvelCharacterName() -> String
    func getMarvelCharacterImageThumbnail() -> String
    func getMarvelCharacterDescription() -> String
}

protocol MarvelCharactersDetailInteractorOutputProtocol: AnyObject {
}

protocol MarvelCharactersDetailRouterProtocol: AnyObject {
    var viewController: MarvelBaseViewController? { get set }
    
    static func createMarvelCharactersDetailModule(characterDetailEntity: CharacterDetailEntity) -> MarvelCharactersDetailViewController
}

protocol MarvelCharactersDetailViewProtocol: AnyObject {
    var presenter: MarvelCharactersDetailPresenterProtocol? { get set }
}

