//
//  MarvelCharactersDetailRouter.swift
//  MarvelWallExample
//
//  Created by Julian Rodrigo Arredondo Escobar on 21/11/21.
//  Copyright (c) 2021 Personal. All rights reserved.
//

import UIKit

final class MarvelCharactersDetailRouter: MarvelCharactersDetailRouterProtocol {
    
    weak var viewController: MarvelBaseViewController?
    
    static func createMarvelCharactersDetailModule(characterDetailEntity: CharacterDetailEntity) -> MarvelCharactersDetailViewController {
        let ref = MarvelCharactersDetailViewController(nibName: "MarvelCharactersDetailViewController", bundle: nil)
        
        let presenter: MarvelCharactersDetailPresenterProtocol & MarvelCharactersDetailInteractorOutputProtocol = MarvelCharactersDetailPresenter()
        
        let router = MarvelCharactersDetailRouter()
        router.viewController = ref
        
        let interactor = MarvelCharactersDetailInteractor()
        interactor.presenter = presenter
        
        presenter.view = ref
        presenter.router = router
        presenter.interactor = interactor
        presenter.characterDetailEntity = characterDetailEntity
        
        ref.presenter = presenter
        return ref
    }
}

