//
//  MarvelDetailCharacterPopAnimator.swift
//  MarvelWallExample
//
//  Created by Julian Rodrigo Arredondo Escobar on 28/11/21.
//  Copyright © 2020 personal. All rights reserved.
//

import Foundation
import UIKit

class MarvelDetailCharacterPopAnimator: NSObject, UIViewControllerAnimatedTransitioning {
    
    private let transitionDuration = 0.3
    private let zoomViewDuration = 0.38
    
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return transitionDuration
    }
    
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        
        let containerView = transitionContext.containerView
        containerView.backgroundColor = UIColor.red
        
        guard let fromVC = transitionContext.viewController(forKey: .from) as? animTransitionable,
            let toVC = transitionContext.viewController(forKey: .to) as? animTransitionable else {
                transitionContext.completeTransition(true)
                return
        }

        let fromViewController = transitionContext.viewController(forKey: .from)!
        let toViewController = transitionContext.viewController(forKey: .to)!

        let viewSnapshot = UIView(frame: fromVC.viewCharacterInfo.frame)
        viewSnapshot.clipsToBounds = true
        viewSnapshot.layer.cornerRadius = 8.0
        viewSnapshot.layer.masksToBounds = true
        viewSnapshot.backgroundColor = fromVC.viewCharacterInfo.backgroundColor

        containerView.addSubview(fromViewController.view)
        containerView.addSubview(toViewController.view)
        containerView.addSubview(viewSnapshot)

        fromViewController.view.isHidden = true
        toViewController.view.isHidden = true

        viewSnapshot.frame = containerView.convert(fromVC.viewCharacterInfo.frame, from: fromVC.viewCharacterInfo.superview)
       
        let zoomViewAnimation = {
            UIViewPropertyAnimator(duration: zoomViewDuration, curve: .easeOut) {
                viewSnapshot.frame = containerView.convert(toVC.viewCharacterInfo.frame, from: toVC.viewCharacterInfo.superview)
            }
        }()

        zoomViewAnimation.startAnimation()
        zoomViewAnimation.addCompletion { _ in
            viewSnapshot.removeFromSuperview()
            fromViewController.view.removeFromSuperview()
            toViewController.view.isHidden = false
            transitionContext.completeTransition(true)
        }
    }
}
