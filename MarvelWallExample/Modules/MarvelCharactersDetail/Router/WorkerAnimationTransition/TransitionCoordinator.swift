//
//  TransitionCoordinator.swift
//  MarvelWallExample
//
//  Created by Julian Rodrigo Arredondo Escobar on 28/11/21.
//  Copyright © 2020 personal. All rights reserved.
//

import Foundation
import UIKit

class TransitionCoordinator: NSObject, UINavigationControllerDelegate {
    internal func navigationController(_ navigationController: UINavigationController,
                              animationControllerFor operation: UINavigationController.Operation,
                              from fromVC: UIViewController,
                              to toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        
        switch operation {
        case .push:
            return MarvelDetailCharacterPushAnimator()
        case .pop:
            return MarvelDetailCharacterPopAnimator()
        default:
            return nil
        }
                
    }
    
}
