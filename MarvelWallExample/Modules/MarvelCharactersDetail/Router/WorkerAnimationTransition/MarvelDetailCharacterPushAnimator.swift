//
//  PushAnimator.swift
//  MarvelWallExample
//
//  Created by Julian Rodrigo Arredondo Escobar on 28/11/21.
//  Copyright © 2020 personal. All rights reserved.
//

import Foundation
import UIKit

protocol animTransitionable {
    var viewCharacterInfo: UIView { get }
}

class MarvelDetailCharacterPushAnimator: NSObject, UIViewControllerAnimatedTransitioning {

    private let transitionDuration = 0.4
    private let zoomViewDuration = 0.4

    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return transitionDuration
    }

    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {

        let containerView = transitionContext.containerView
        containerView.backgroundColor = UIColor.red

        guard let fromVC = transitionContext.viewController(forKey: .from) as? animTransitionable,
            let toVC = transitionContext.viewController(forKey: .to) as? animTransitionable else {
                transitionContext.completeTransition(true)
                return
        }

        let fromViewController = transitionContext.viewController(forKey: .from)!
        let toViewController = transitionContext.viewController(forKey: .to)!

        let viewSnapshot = UIView(frame: fromVC.viewCharacterInfo.frame)
        viewSnapshot.backgroundColor = fromVC.viewCharacterInfo.backgroundColor

        containerView.addSubview(fromViewController.view)
        containerView.addSubview(toViewController.view)

        containerView.addSubview(viewSnapshot)

        fromViewController.view.isHidden = true
        toViewController.view.isHidden = true

        viewSnapshot.frame = containerView.convert(fromVC.viewCharacterInfo.frame, from: fromVC.viewCharacterInfo.superview)
       
        let zoomViewAnimation = {
            UIViewPropertyAnimator(duration: zoomViewDuration, curve: .easeOut) {
                viewSnapshot.frame = toVC.viewCharacterInfo.frame
            }
        }()

        zoomViewAnimation.startAnimation()
        zoomViewAnimation.addCompletion { _ in
            viewSnapshot.removeFromSuperview()
            fromViewController.view.removeFromSuperview()
            toViewController.view.isHidden = false
            transitionContext.completeTransition(true)
        }
    }
}
