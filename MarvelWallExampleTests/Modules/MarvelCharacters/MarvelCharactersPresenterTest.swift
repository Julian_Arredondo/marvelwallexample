//
//  MarvelCharactersPresenterTest.swift
//  MarvelWallExampleTests
//
//  Created by Julian Rodrigo Arredondo Escobar on 28/11/21.
//

import Foundation

import XCTest
@testable import MarvelWallExample

final class  MarvelCharactersPresenterTest: XCTestCase {
    
    var mockView: MockView!
    var mockRouter: MockRouter!
    var mockInteractor: MockInteractor!
    var sut: MarvelCharactersPresenter!
    
    override func setUp() {
        super.setUp()
        mockView = MockView()
        mockRouter = MockRouter()
        mockInteractor = MockInteractor()
        
        sut = MarvelCharactersPresenter()
        sut.view = mockView
        sut.router = mockRouter
        sut.interactor = mockInteractor
    }
    
    override func tearDown() {
        super.tearDown()
        mockView = nil
        mockRouter = nil
        mockInteractor = nil
        sut = nil
    }
    
    func test_viewDidLoad() {
        // When
        sut.viewDidLoad()
        
        // Then
        XCTAssertTrue(mockInteractor.getMarvelCharactersCalled)
        XCTAssertTrue(mockView.showLoaderViewCalled)
    }
    
    func test_GetNumberOfCharacter() {
        //Given
        sut.marvelCharacterArrayModel = DummyData.marvelCharacterArrayModel
        
        // When // Then
        XCTAssertEqual(sut.getNumberOfCharacter(), DummyData.marvelCharacterArrayModel.count)
    }
    
    func test_OnFetchMarvelCharactersSuccess() {
        //Given
        let mockMarvelCharacterResultModel = MarvelCharacterResultModel(
            characters:DummyData.marvelCharacterArrayModel)
        
        // When
        sut.onFetchMarvelCharactersSuccess(marvelCharacterModel: mockMarvelCharacterResultModel)
        
        // Then
        XCTAssertTrue(mockView.reloadInfoCalled)
    }
    
    func test_ResetPagination() {
        //Given
        sut.marvelCharacterArrayModel = DummyData.marvelCharacterArrayModel
        
        // When
        sut.resetPagination()
        
        // Then
        XCTAssertEqual(0, sut.marvelCharacterArrayModel.count)
    }
    
    func test_GetCharacterByIndex() {
        //Given
        sut.marvelCharacterArrayModel = DummyData.marvelCharacterArrayModel
        
        // When
        let character = sut.getCharacterByIndex(index: .zero)?.id
        
        // Then
        XCTAssertEqual(character, DummyData.marvelCharacterArrayModel[.zero].id)
    }
    
    func test_GetCharacterByIndex_Empty() {
        //Given
        sut.marvelCharacterArrayModel = DummyData.marvelCharacterEmptyArrayModel
        
        // When
        let character = sut.getCharacterByIndex(index: .zero)?.id
        
        // Then
        XCTAssertNil(character)
    }
    
    func test_GetSearchText() {
        //Given
        let search = "Iron"
        sut.setSearchText(text: search)
        
        // When
        let searchextTest = sut.getSearchText()
        
        // Then
        XCTAssertEqual(search, searchextTest)
    }
    
    func test_FetchCharacter() {
        // When
        sut.fetchMarvelCharacters(nameStartsWith: "", offset: .zero)
        
        // Then
        XCTAssertTrue(mockInteractor.getMarvelCharactersCalled)
    }
    
    func test_FetchCharacterDetail() {
        // When
        sut.fetchCharacterDetail(id: 0)
        
        // Then
        XCTAssertTrue(mockInteractor.getMarvelCharactersByIDCalled)
    }
    
    func  test_GoToCharacterDetail() {
        // When
        sut.goToCharacterDetail(characterDetailEntity: DummyData.marvelCharacterEntity)
        
        // Then
        XCTAssertTrue(mockRouter.gotToChacarterDetailCalled)
    }
    
    func test_OnFetchMarvelCharactersDetailSuccess() {
        // When
        sut.onFetchMarvelCharactersDetailSuccess(characterDetailEntity: DummyData.marvelCharacterEntity)
        
        // Then
        XCTAssertTrue(mockView.hideLoaderViewCalled)
        XCTAssertTrue(mockRouter.gotToChacarterDetailCalled)
    }
     
    class MockView: MarvelCharactersViewProtocol {
        var presenter: MarvelCharactersPresenterProtocol?
        
        var reloadInfoCalled = false
        var showLoaderViewCalled = false
        var hideLoaderViewCalled = false
        
        func reloadInfo() {
            reloadInfoCalled = true
        }
        
        func showLoaderView() {
            showLoaderViewCalled = true
        }
        
        func hideLoaderView() {
            hideLoaderViewCalled = true
        }
    }
    
    final  class MockRouter: MarvelCharactersRouterProtocol {
        var viewController: MarvelBaseViewController?
        var gotToChacarterDetailCalled = false
        
        static func createMarvelCharactersModule() -> MarvelCharactersViewController {
            return MarvelCharactersViewController()
        }
        
        func gotToChacarterDetail(characterDetailEntity: CharacterDetailEntity) {
            gotToChacarterDetailCalled = true
        }
    }
    
    class MockInteractor: MarvelCharactersInteractorProtocol {
        var presenter: MarvelCharactersInteractorOutputProtocol?
        var getMarvelCharactersCalled = false
        var getMarvelCharactersByIDCalled = false
        
        func getMarvelCharacters(nameStartsWith: String, offset: Int) {
            getMarvelCharactersCalled = true
        }
        
        func getMarvelCharactersByID(id: Int) {
            getMarvelCharactersByIDCalled = true
        }
    }
    
    struct DummyData {
        static let marvelCharacterArrayModel = [MarvelCharacterModel(
                                                    id: 0,
                                                    name: "marvelName1",
                                                    description: "marvelDescription1",
                                                    thumbnail: nil),
                                                MarvelCharacterModel(
                                                    id: 1,
                                                    name: "marvelName2",
                                                    description: "marvelDescription2",
                                                    thumbnail: nil),
                                                MarvelCharacterModel(
                                                    id: 2,
                                                    name: "marvelName3",
                                                    description: "marvelDescription3",
                                                    thumbnail: nil)]
        
        static let marvelCharacterEntity = CharacterDetailEntity(
            name: "marvelName1",
            description: "marvelDescription1",
            thumbnail: nil,
            sections: [CharacterDetailEntity.SectionsCharacterInfo(
                        items: ["item1"],
                        nameSection: "name1")])
        
        static let marvelCharacterEmptyArrayModel = [MarvelCharacterModel]()
    }
}
