//
//  MarvelCharactersInteractorTest.swift
//  MarvelWallExampleTests
//
//  Created by Julian Rodrigo Arredondo Escobar on 28/11/21.
//

import Foundation

import XCTest
@testable import MarvelWallExample

final class HomePocketInteractorTest: XCTestCase {
    
    let gettMarvelCharacter = "GetMarvelCharacter"
    let getMarvelCharacterNotResult = "GetMarvelCharacterNotResult"
    
    var mockPresenter: MockPresenter!
    var sut: MarvelCharactersInteractor!
    
    override func setUp() {
        super.setUp()
        mockPresenter = MockPresenter()
        sut = MarvelCharactersInteractor()
        sut.presenter = mockPresenter
    }
    
    override func tearDown() {
        mockPresenter = nil
        sut = nil
        super.tearDown()
    }
    
    func test_GetMarvelCharacters() {
        //Given
        setupAdapter(file: gettMarvelCharacter, code: 200, error: nil)
        
        //When
        sut.getMarvelCharacters(nameStartsWith: "", offset: 0)
        
        //Then
        XCTAssert(mockPresenter.onFetchMarvelCharactersSuccessCalled)
    }
    
    func test_getMarvelCharacters_failure() {
        //Given
        setupAdapter(file: gettMarvelCharacter, code: 404, error: nil)
        
        //When
        sut.getMarvelCharacters(nameStartsWith: "", offset: 0)
        
        //Then
        XCTAssert(mockPresenter.onFetchMarvelCharactersErrorCalled)
    }
    
    func test_GetMarvelCharactersDetail() {
        //Given
        setupAdapter(file: gettMarvelCharacter, code: 200, error: nil)
        
        //When
        sut.getMarvelCharactersByID(id: 0)
        
        //Then
        XCTAssert(mockPresenter.onFetchMarvelCharactersDetailSuccessCalled )
    }
    
    func test_getMarvelCharactersDetail_failure() {
        //Given
        setupAdapter(file: gettMarvelCharacter, code: 404, error: nil)
        
        //When
        sut.getMarvelCharactersByID(id: 0)
        
        //Then
        XCTAssert(mockPresenter.onFetchMarvelCharactersDetailErrorCalled)
    }
     
    private func setupAdapter(file: String, code: Int, error: Error? = nil) {
        let router = MockRouter.shared.createMockSession(fromJsonFile: file,
                                                         andStatusCode: code,
                                                         andError: error,
                                                         as: NetworkingMarvelCharactersAPI.self)
        let service = NetworkingMarvelCharactersRouter(router)
        
        sut.networkingMarvelCharactersAdapter = NetworkingMarvelCharactersAdapter(service)
    }
    
    private func setupNetworkingMarvelCharactersAdapter(file: String = String(), code: Int, error: Error? = nil) {
        let router = MockRouter.shared.createMockSession(fromJsonFile: file, andStatusCode: code, andError: error, as: NetworkingMarvelCharactersAPI.self)
        let service = NetworkingMarvelCharactersRouter(router)
        sut.networkingMarvelCharactersAdapter = NetworkingMarvelCharactersAdapter(service)
    }
    
    class MockPresenter:MarvelCharactersInteractorOutputProtocol {
        
        var onFetchMarvelCharactersSuccessCalled = false
        var onFetchMarvelCharactersErrorCalled = false
        var onFetchMarvelCharactersDetailSuccessCalled = false
        var onFetchMarvelCharactersDetailErrorCalled = false
        
        func onFetchMarvelCharactersSuccess(marvelCharacterModel: MarvelCharacterResultModel) {
            onFetchMarvelCharactersSuccessCalled = true
        }
        
        func onFetchMarvelCharactersError() {
            onFetchMarvelCharactersErrorCalled = true
        }
        
        func onFetchMarvelCharactersDetailSuccess(characterDetailEntity: CharacterDetailEntity) {
            onFetchMarvelCharactersDetailSuccessCalled = true
        }
        
        func onFetchMarvelCharactersDetailError() {
            onFetchMarvelCharactersDetailErrorCalled = true
        }
    }
}
