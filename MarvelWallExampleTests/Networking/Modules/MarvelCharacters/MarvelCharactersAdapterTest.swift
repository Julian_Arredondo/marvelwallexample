//
//  MarvelCharactersAdapterTest.swift
//  MarvelWallExampleTests
//
//  Created by Julian Rodrigo Arredondo Escobar on 28/11/21.
//

import XCTest
@testable import MarvelWallExample

class NetworkingTransactionsLimitsAdapterTest: XCTestCase {

    let gettMarvelCharacter = "GetMarvelCharacter"
    let getMarvelCharacterNotResult = "GetMarvelCharacterNotResult"
    var sut: NetworkingMarvelCharactersAdapter!
    
    override func setUp() {
        super.setUp()
        sut = NetworkingMarvelCharactersAdapter()
    }
    
    override func tearDown() {
        super.tearDown()
        sut = nil
    }

    func test_ServiceSuccess_GetCharacters() {
        // Given
        setupNetworkingCharacterAdapter(file: gettMarvelCharacter, code: 200)

        // When // Then
        sut.getCharacters(nameStartsWith: "", offset: .zero, completion: { (response) in
            switch response {
            case .success(let data):
                XCTAssertNotNil(data)
            case .failure(let error):
                XCTAssertNil(error)
            }
        })
    }
    
    func test_ServiceInvalid_GetCharacters() {
        // Given
        setupNetworkingCharacterAdapter(file: "INVALID", code: 200)

        // When // Then
        sut.getCharacters(nameStartsWith: "", offset: .zero, completion: { (response) in
            switch response {
            case .success(let data):
                XCTAssertNil(data)
            case .failure(let error):
                XCTAssertNotNil(error)
            }
        })
    }
    
    func test_ServiceNameNotResult_GetCharacters() {
        // Given
        setupNetworkingCharacterAdapter(file: getMarvelCharacterNotResult, code: 200)

        // When // Then
        sut.getCharacters(nameStartsWith: "noMarvelName", offset: .zero, completion: { (response) in
            switch response {
            case .success(let data):
                XCTAssertNotNil(data)
                XCTAssertEqual(.zero, data.characters?.count)
            case .failure(let error):
                XCTAssertNil(error)
            }
        })
    }
    
    func test_ServiceError_GetCharacters() {
        // Given
        setupNetworkingCharacterAdapter(file: gettMarvelCharacter, code: 403)

        // When // Then
        sut.getCharacters(nameStartsWith: "", offset: .zero, completion: { (response) in
            switch response {
            case .success(let data):
                XCTAssertNil(data)
            case .failure(let error):
                XCTAssertNotNil(error)
            }
        })
    }

    private func setupNetworkingCharacterAdapter(file: String = String(), code: Int, error: Error? = nil) {
        let router = MockRouter.shared.createMockSession(fromJsonFile: file, andStatusCode: code, andError: error, as: NetworkingMarvelCharactersAPI.self)
        let service = NetworkingMarvelCharactersRouter(router)
        sut = NetworkingMarvelCharactersAdapter(service)
    }
}
