//
//  MockRouterMarvel.swift
//  MarvelWallExampleTests
//
//  Created by Julian Rodrigo Arredondo Escobar on 28/11/21.
//

import XCTest
@testable import MarvelWallExample

class MockRouterMarvel<EndPoint: EndPointType>: NetworkRouter {
    var lastFailedEndPoint: EndPoint?
    var completionHandler: (Data?, HTTPURLResponse?, Error?)
    private var printEndPointData: Bool = false
    
    required init() { }
    
    convenience init(completionHandler: (Data?, HTTPURLResponse?, Error?), printEndPointData: Bool = false) {
        self.init()
        self.printEndPointData = printEndPointData
        self.completionHandler = completionHandler
    }
    
    func request(_ route: EndPoint, completion: @escaping NetworkRouterCompletion) {
        switch completionHandler.1?.statusCode ?? .zero {
        case 200...299:
            break
        default:
            lastFailedEndPoint = route
        }
        completion(self.completionHandler.0, self.completionHandler.1, self.completionHandler.2)
    }
}

class MockRouter {
    
    static var shared = MockRouter()
    
    private init() {}
    
    private func loadJsonData(file: String) -> Data? {
        if let jsonFilePath = Bundle(for: type(of: self)).path(forResource: file, ofType: "json") {
            let jsonFileURL = URL(fileURLWithPath: jsonFilePath)
            
            if let jsonData = try? Data(contentsOf: jsonFileURL) {
                return jsonData
            }
        }
        return nil
    }
    
    func createMockSession<T: EndPointType>(fromJsonFile file: String = String(),
                                            andStatusCode code: Int,
                                            andError error: Error? = nil, as type: T.Type = T.self,
                                            headers: HTTPHeaders? = nil) -> MockRouterMarvel<T> {
        
        let data = file.isEmpty ? nil : loadJsonData(file: file)
        let response = HTTPURLResponse(
            url: URL(string: "TESTUrl")!,
            statusCode: code,
            httpVersion: nil,
            headerFields: headers)
        return MockRouterMarvel<T>(completionHandler: (data, response, error), printEndPointData: true)
    }
}
